package com.borovik.app;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


/**
 * Created by dev3 on 2/19/14.
 */

public class CriminalDB extends SQLiteOpenHelper {

    SQLiteDatabase db;

    private static final String DATABASE_NAME = "Criminal_database.db";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_DETENTION = "detention";
    private static final String D_ID = "_det_id";
    private static final String DETENTION="detention";
    private static final String DATE="date";
    private static final String OFFICER_ID="of_id";
    private static final String DISTRICT_ID="dis_id";
    private static final String SUSPECTS_ID="sus_id";

    private static final String TABLE_OFFICER="officer";
    private static final String O_ID="_of_id";
    private static final String O_NAME="name";
    private static final String O_GENDER="gender";
    private static final String O_AGE="age";
    private static final String O_RANK="rank";

    private static final String TABLE_DISTRICT="district";
    private static final String DIS_ID="_dis_id";
    private static final String DIS_NAME="name_dis";
    private static final String ADDRESS="address";

    private static final String TABLE_SUSPECTS="suspects";
    private static final String SUS_ID="_sus_id";
    private static final String SUS_NAME="name";
    private static final String SUS_GENDER="gender";
    private static final String SUS_AGE="age";

    private static final String SQL_CREATE_OFFICER =
    "CREATE TABLE " + TABLE_OFFICER
                    + "("
                    + O_ID + "INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + O_NAME + "TEXT NOT NULL,"
                    + O_GENDER + "TEXT NOT NULL,"
                    + O_AGE + "TEXT NOT NULL,"
                    + O_RANK + "TEXT NOT NULL);";

    private static final String SQL_CREATE_DISTRICT =
            "CREATE TABLE " + TABLE_DISTRICT
                    + "("
                    + DIS_ID + "INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + DIS_NAME + "TEXT NOT NULL,"
                    + ADDRESS + "TEXT NOT NULL);";

    private static final String SQL_CREATE_SUSPECTS =
            "CREATE TABLE " + TABLE_SUSPECTS
                    + "("
                    + SUS_ID + "INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + SUS_NAME + "TEXT NOT NULL,"
                    + SUS_AGE +"TEXT NOT NULL,"
                    + SUS_GENDER + "TEXT NOT NULL);";

    private static final String SQL_CREATE_DETENTION =
            "CREATE TABLE " + TABLE_DETENTION
                    + "("
                    + D_ID + "INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + DETENTION + "TEXT NOT NULL,"
                    + DATE + "TEXT NOT NULL,"
                    + OFFICER_ID + "INTEGER NOT NULL,FOREIGN KEY ("+OFFICER_ID+") REFERENCES "+TABLE_OFFICER+"("+O_ID+") ON DELETE CASCADE ON UPDATE CASCADE,"
                    + DISTRICT_ID +"INTEGER NOT NULL,FOREIGN KEY ("+DISTRICT_ID+") REFERENCES "+TABLE_DISTRICT+"("+DIS_ID+") ON DELETE CASCADE ON UPDATE CASCADE,"
                    + SUSPECTS_ID + "INTEGER NOT NULL,FOREIGN KEY ("+SUSPECTS_ID+") REFERENCES "+TABLE_SUSPECTS+"("+SUS_ID+") ON DELETE CASCADE ON UPDATE CASCADE);";

    private static final String SQL_DELETE_OFFICER = "DROP TABLE IF EXISTS "
            + TABLE_OFFICER;

    private static final String SQL_DELETE_DISTRICT = "DROP TABLE IF EXISTS "
            + TABLE_DISTRICT;

    private static final String SQL_DELETE_SUSPECTS = "DROP TABLE IF EXISTS "
            + TABLE_SUSPECTS;

    private static final String SQL_DELETE_DETENTION = "DROP TABLE IF EXISTS "
            + TABLE_DETENTION;

    public CriminalDB(Context context){

        super(context,DATABASE_NAME,null,DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_OFFICER);
        db.execSQL(SQL_CREATE_DISTRICT);
        db.execSQL(SQL_CREATE_SUSPECTS);
        db.execSQL(SQL_CREATE_DETENTION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(CriminalDB.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");

        db.execSQL(SQL_DELETE_OFFICER);
        db.execSQL(SQL_DELETE_DISTRICT);
        db.execSQL(SQL_DELETE_SUSPECTS);
        db.execSQL(SQL_DELETE_DETENTION);

        onCreate(db);
    }

    public void addOfficer(String o_NAME,String o_RANK,String o_AGE,String o_GENDER){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(O_NAME,o_NAME);
        cv.put(O_RANK,o_RANK);
        cv.put(O_AGE,o_AGE);
        cv.put(O_GENDER,o_GENDER);
        db.insert(TABLE_OFFICER,null,cv);

    }

    public void updateOfficer(long _of_id,String o_NAME,String o_RANK,String o_AGE,String o_GENDER){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(O_NAME,o_NAME);
        cv.put(O_RANK,o_RANK);
        cv.put(O_AGE,o_AGE);
        cv.put(O_GENDER,o_GENDER);
        String where=O_ID +"="+_of_id;
        db.update(TABLE_OFFICER,cv,where,null);

    }

    public void deleteOfficer(long _of_id){
        SQLiteDatabase db=this.getWritableDatabase();
        db.delete(TABLE_OFFICER,O_ID +"="+_of_id,null);
    }

    public void addDistrict(String name_dis,String address){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(DIS_NAME,name_dis);
        cv.put(ADDRESS,address);
        db.insert(TABLE_DISTRICT,null,cv);

    }

    public void updateDistrict(long _dis_id,String name_dis,String address){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(DIS_NAME,name_dis);
        cv.put(ADDRESS,address);
        String where=DIS_ID +"="+_dis_id;
        db.update(TABLE_DISTRICT, cv, where, null);

    }

    public void deleteDistrict(long _dis_id){
        SQLiteDatabase db=this.getWritableDatabase();
        db.delete(TABLE_DISTRICT,DIS_ID +"="+_dis_id,null);

    }

    public void addSuspects(String name,String gender,String age){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(SUS_NAME,name);
        cv.put(SUS_GENDER,gender);
        cv.put(SUS_AGE,age);
        db.insert(TABLE_SUSPECTS,null,cv);

    }

    public void updateSuspects(long _sus_id,String name,String gender,String age){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(SUS_NAME,name);
        cv.put(SUS_GENDER,gender);
        cv.put(SUS_AGE,age);
        String where=SUS_ID +"="+_sus_id;
        db.update(TABLE_SUSPECTS, cv, where, null);

    }

    public void deleteSuspects(long _sus_id){
        SQLiteDatabase db=this.getWritableDatabase();
        db.delete(TABLE_SUSPECTS,SUS_ID +"="+_sus_id,null);

    }

    public void addDetention(String detention,String date,String of_id,String dis_id,String sus_id){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(DETENTION,detention);
        cv.put(DATE,date);
        cv.put(OFFICER_ID,of_id);
        cv.put(DISTRICT_ID,dis_id);
        cv.put(SUSPECTS_ID,sus_id);
        db.insert(TABLE_DETENTION,null,cv);
    }

    public void updateDetention(long _det_id,String detention,String date,String of_id,String dis_id,String sus_id){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(DETENTION,detention);
        cv.put(DATE,date);
        cv.put(OFFICER_ID,of_id);
        cv.put(DISTRICT_ID,dis_id);
        cv.put(SUSPECTS_ID,sus_id);
        String where=D_ID +"="+_det_id;
        db.update(TABLE_DETENTION, cv, where, null);
    }

    public void deleteDetetion(long _det_id){
        SQLiteDatabase db=this.getWritableDatabase();
        db.delete(TABLE_DETENTION,D_ID +"="+_det_id,null);

    }

    public void onOpenDB(){
        super.onOpen(db);
              if(!db.isReadOnly()){
                  db.execSQL("PRAGMA foreign_keys=ON;");
              }
    }

    public void closeDB(){
        SQLiteDatabase db=this.getReadableDatabase();
        if(db!=null && db.isOpen()){
              db.close();
        }
    }
}
