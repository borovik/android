package com.borovik.app;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

/**
 * Created by dev3 on 2/19/14.
 */

public class MainActivity extends FragmentActivity implements LoaderCallbacks<Cursor> {

    CriminalDB criminalDB;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        criminalDB=new CriminalDB(this);
        criminalDB.onOpenDB();

    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {

    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    static class MyCursorLoader extends CursorLoader{

        public MyCursorLoader(Context context,CriminalDB criminalDB1){
            super(context);

        }

    @Override
    public Cursor  loadInBackgroud(){
     }
   }
}
