package com.borovyk.app;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

/**
 * Created by dev3 on 2/13/14.
 */
public class MainActivity extends Activity {

    MyTask mt;
    TextView textView;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        textView = (TextView) findViewById(R.id.txView);

        mt = (MyTask) getLastNonConfigurationInstance();
        if (mt == null) {
            mt = new MyTask();
            mt.execute();
        }
        mt.link(this);
    }


    static class MyTask extends AsyncTask<String, Integer, Void> {

        MainActivity activity;


      void link(MainActivity act){
          activity=act;
      }

      void unLink(){
          activity=null;
      }

    @Override
    protected Void doInBackground(String... params) {
         try {
            for (int i = 1; i <= 10; i++) {
              TimeUnit.SECONDS.sleep(1);
              publishProgress(i);
                }
              } catch (InterruptedException e) {
                    e.printStackTrace();
            }
                return null;
         }


     @Override
     protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);
                activity.textView.setText("i = " + values[0]);
            }
    }

    public Object onRetainNonConfigurationInstance() {
        mt.unLink();
        return mt;
    }
}
