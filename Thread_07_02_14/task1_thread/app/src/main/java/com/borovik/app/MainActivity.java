package com.borovik.app;

import android.app.Activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends Activity{

    Handler h;
    EditText edText;
    Button button;
    int fact;
    int calc_fact;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        edText = (EditText) findViewById(R.id.edText);
        button = (Button) findViewById(R.id.button);


        h = new Handler() {
            public void handleMessage(android.os.Message msg) {
                edText.setText("Factorial: " + msg.arg1);
            };
        };
    }

    public void onclick(View v) {

        fact = Integer.parseInt(edText.getText().toString());

        switch (v.getId()) {

            case R.id.button:

                Thread t = new Thread(new Runnable() {

                    Message msg;

                    public void run() {

                        calc_fact=Factorial();
                        msg = h.obtainMessage(0,calc_fact,0);
                        h.sendMessage(msg);
                    }
                });
                t.start();
                break;
            default:
                break;
        }
    }

   public int Factorial() {

        int ret = 1;
        for (int i = 1; i <= fact; ++i) ret *= i;
        return ret;
    }
}
