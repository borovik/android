package com.borovyk.app;


import android.content.Context;
import android.os.Bundle;
import android.content.AsyncTaskLoader;
import android.widget.EditText;

/**
 * Created by dev3 on 11.02.14.
 */

public class MyLoader extends AsyncTaskLoader<Integer> {

   int fact;
   int calc_fact;

    public MyLoader(Context context,Bundle args) {
        super(context);
        calc_fact=args.getInt("Number",fact);
    }

    @Override
    public Integer loadInBackground() {
        int ret=1;
        for (int i = 1; i <= calc_fact; ++i) ret *= i;
        return ret;
    }
}
