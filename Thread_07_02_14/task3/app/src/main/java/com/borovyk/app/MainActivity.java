package com.borovyk.app;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by dev3 on 11.02.14.
 */

public class MainActivity extends Activity implements View.OnClickListener,LoaderManager.LoaderCallbacks<Integer>{

    int fact;
    static final int ID_LOADER=1;

    EditText editText;
    Button button;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        editText = (EditText) findViewById(R.id.etInfo);
        button=(Button)findViewById(R.id.btn1);
        button.setOnClickListener(this);

    }

    @Override
    public Loader<Integer> onCreateLoader(int i, Bundle args) {

        Loader<Integer> loader;
        loader=new MyLoader(this,args);
        return loader;
    }

    @Override
    public void onClick(View view) {


        fact=Integer.parseInt(editText.getText().toString());
        Bundle bndl= new Bundle();
        bndl.putInt("Number",fact);
        getLoaderManager().initLoader(ID_LOADER, bndl, this).reset();

        if(fact>=1){
            Loader<Integer> loader;
            loader=getLoaderManager().getLoader(ID_LOADER);
            loader.forceLoad();
        }
    }

    @Override
    public void onLoadFinished(android.content.Loader<Integer> integerLoader, Integer ret) {
        editText.setText("Factorial : "+ret);
    }

    @Override
    public void onLoaderReset(android.content.Loader<Integer> integerLoader) {
    }
}


