package com.borovyk.app;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


/**
 * Created by Уэйн on 11.02.14.
 */
public class MainActivity extends Activity implements View.OnClickListener {

    MyTask mt;
    EditText etInfo;
    Button button;
    int fact;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        etInfo = (EditText) findViewById(R.id.etInfo);
        button=(Button)findViewById(R.id.btn1);
        button.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        fact = Integer.parseInt(etInfo.getText().toString());
        mt = new MyTask();
        mt.execute();
    }

    class MyTask extends AsyncTask<Integer, Integer, Integer> {


        @Override
        protected Integer doInBackground(Integer... integers) {

            int ret = 1;
            for (int i = 1; i <= fact; ++i) ret *= i;
            return ret;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Integer ret) {
            super.onPostExecute(ret);

        }
    }
}
