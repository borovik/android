package com.borovik.app;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by dev3 on 2/13/14.
 */
public class MainActivity extends Activity implements View.OnClickListener {

    Button button;
    int count=0;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        button=(Button)findViewById(R.id.btn1);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View view){

        Toast.makeText(this, "Count = " + ++count, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putInt("count", count);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
        count=savedInstanceState.getInt("Count");
    }
}
