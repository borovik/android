package com.example.app;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by dev3 on 2/12/14.
 */
public class MainActivity extends Activity implements View.OnClickListener {

    Button button;

    @Override
    public  void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        button=(Button)findViewById(R.id.button);
        button.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Intent intent=new Intent();
        intent.setAction("android.intent.action.MyActionActivity");
        intent.addCategory("android.intent.category.MyAction");
        startActivity(intent);
    }
}
