package com.example.app;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by dev3 on 2/12/14.
 */
public class ActionActivity extends Activity {


    @Override
    public  void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.action);

        Intent intent=getIntent();
        String action=intent.getAction();


        if(action.equals("android.intent.action.MyActionActivity")){

           TextView textView=(TextView)findViewById(R.id.textV);
           textView.setText("Action_Activity");
        }

    }
}
