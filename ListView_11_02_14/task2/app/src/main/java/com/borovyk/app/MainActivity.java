package com.borovyk.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


/**
 * Created by dev3 on 2/17/14.
 */

public class MainActivity extends Activity implements View.OnClickListener {

    Button button1;
    Button button2;
    Button button3;
    Button button4;

    public final static int LIST_VIEW=1;
    public final static int GRID_VIEW=2;
    public final static int SPINNER=3;
    public final static int EXPANDABLE_LIST_VIEW=4;



    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        button1=(Button)findViewById(R.id.btn1);
        button1.setOnClickListener(this);
        button2=(Button)findViewById(R.id.btn2);
        button2.setOnClickListener(this);
        button3=(Button)findViewById(R.id.btn3);
        button3.setOnClickListener(this);
        button4=(Button)findViewById(R.id.btn4);
        button4.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {

        Bundle b=new Bundle();

        switch(view.getId()){

            case R.id.btn1:
                Intent intent1=new Intent(this,ListActivity.class);
                b.putInt("key",LIST_VIEW);
                intent1.putExtras(b);
                startActivity(intent1);
                break;

            case R.id.btn2:
                Intent intent2=new Intent(this,ListActivity.class);
                b.putInt("key",GRID_VIEW);
                intent2.putExtras(b);
                startActivity(intent2);
                break;

            case R.id.btn3:
                Intent intent4=new Intent(this,ListActivity.class);
                b.putInt("key",EXPANDABLE_LIST_VIEW);
                intent4.putExtras(b);
                startActivity(intent4);
                break;

            case R.id.btn4:
                Intent intent3=new Intent(this,ListActivity.class);
                b.putInt("key",SPINNER);
                intent3.putExtras(b);
                startActivity(intent3);
                break;
        }
    }
}

