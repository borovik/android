package com.borovyk.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by dev3 on 2/11/14.
 */

public class MyAdapterSpinner extends BaseAdapter {

    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Name> objects;

    MyAdapterSpinner(Context context, ArrayList<Name> names) {
        ctx = context;
        objects = names;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    static class ViewHolder{

        TextView txFName;
        TextView txSName;
        TextView txSurName;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.item, parent, false);

            ViewHolder holder=new ViewHolder();

            holder.txFName=(TextView) view.findViewById(R.id.txFName);
            holder.txSName=(TextView) view.findViewById(R.id.txSName);
            holder.txSurName=(TextView) view.findViewById(R.id.txSurName);
            view.setTag(holder);
        }

        Name p = getName(position);
        if(p !=null){
            ViewHolder holder=(ViewHolder)view.getTag();
            holder.txFName.setText(p.first_name);
            holder.txSName.setText(p.second_name);
            holder.txSurName.setText(p.surname);
        }
        return view;
    }

    Name getName(int position) {
        return ((Name) getItem(position));
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
