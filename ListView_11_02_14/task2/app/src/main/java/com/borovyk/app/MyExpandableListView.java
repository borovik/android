package com.borovyk.app;

import java.util.List;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;

public class MyExpandableListView extends BaseExpandableListAdapter {

     Context mContext;
     ExpandableListView mExpandableListView;
     List<GroupEntity> mGroupCollection;
     int[] groupStatus;

    public MyExpandableListView(Context pContext,
                                ExpandableListView pExpandableListView,
                                List<GroupEntity> pGroupCollection) {
        mContext = pContext;
        mGroupCollection = pGroupCollection;
        mExpandableListView = pExpandableListView;
        groupStatus = new int[mGroupCollection.size()];

        setListEvent();
    }

    private void setListEvent() {

        mExpandableListView
                .setOnGroupExpandListener(new OnGroupExpandListener() {

                    @Override
                    public void onGroupExpand(int arg0) {
                        groupStatus[arg0] = 1;
                    }
                });

        mExpandableListView
                .setOnGroupCollapseListener(new OnGroupCollapseListener() {

                    @Override
                    public void onGroupCollapse(int arg0) {
                        groupStatus[arg0] = 0;
                    }
                });
    }

    @Override
    public String getChild(int arg0, int arg1) {
        return mGroupCollection.get(arg0).GroupItemCollection.get(arg1).Name;
    }

    @Override
    public long getChildId(int arg0, int arg1) {
        return 0;
    }

    @Override
    public View getChildView(int arg0, int arg1, boolean arg2, View arg3,
                             ViewGroup arg4) {
        ChildHolder childHolder;
        if (arg3 == null) {
            arg3 = LayoutInflater.from(mContext).inflate(R.layout.item, null);

            childHolder = new ChildHolder();

            childHolder.title = (TextView) arg3.findViewById(R.id.txSurName);
            arg3.setTag(childHolder);
        }else {
            childHolder = (ChildHolder) arg3.getTag();
        }

        childHolder.title.setText(mGroupCollection.get(arg0).GroupItemCollection.get(arg1).Name);
        return arg3;
    }

    @Override
    public int getChildrenCount(int arg0) {
        return mGroupCollection.get(arg0).GroupItemCollection.size();
    }

    @Override
    public Object getGroup(int arg0) {
        return mGroupCollection.get(arg0);
    }

    @Override
    public int getGroupCount() {
        return mGroupCollection.size();
    }

    @Override
    public long getGroupId(int arg0) {
        return arg0;
    }

    @Override
    public View getGroupView(int arg0, boolean arg1, View arg2, ViewGroup arg3) {
        GroupHolder groupHolder;
        if (arg2 == null) {
            arg2 = LayoutInflater.from(mContext).inflate(R.layout.group,null);
            groupHolder = new GroupHolder();
            groupHolder.title = (TextView) arg2.findViewById(R.id.textView1);
            arg2.setTag(groupHolder);
        } else {
            groupHolder = (GroupHolder) arg2.getTag();
        }
        groupHolder.title.setText(mGroupCollection.get(arg0).Name);

        return arg2;
    }

    class GroupHolder {
        TextView title;
    }

    class ChildHolder {
        TextView title;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int arg0, int arg1) {
        return true;
    }
}
