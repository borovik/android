package com.borovyk.app;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Spinner;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dev3 on 2/18/14.
 */

public class ListActivity extends Activity  {

    ArrayList<Name> names=new ArrayList<Name>();
    List<GroupEntity> mGroupCollection;


    MyAdapterListView myAdapterListView;
    MyAdapterGriedView myAdapterGriedView;
    MyAdapterSpinner myAdapterSpinner;
    ExpandableListView mExpandableListView;

    public final static int LIST_VIEW=1;
    public final static int GRID_VIEW=2;
    public final static int SPINNER=3;
    public final static int EXPANDABLE_LIST_VIEW=4;

    @Override
    public  void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_activity);

        ListView ltView = (ListView) findViewById(R.id.ltView);
        GridView gridView=(GridView)findViewById(R.id.gdView);
        Spinner spinner = (Spinner) findViewById(R.id.spin);
        spinner.setEnabled(true);

      Bundle b = getIntent().getExtras();

    switch (b.getInt("key")){

        case LIST_VIEW:

            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            myAdapterListView = new MyAdapterListView(this, names);
            ltView.setAdapter(myAdapterListView);
            break;

        case GRID_VIEW:

            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            myAdapterGriedView=new MyAdapterGriedView(this,names);
            gridView.setAdapter(myAdapterGriedView);
            break;

        case EXPANDABLE_LIST_VIEW:

            addData();
            mExpandableListView = (ExpandableListView) findViewById(R.id.eltView);
            MyExpandableListView myExpandableListView = new MyExpandableListView(this,mExpandableListView, mGroupCollection);
            mExpandableListView.setAdapter(myExpandableListView);
            break;

        case SPINNER:

            spinner.setEnabled(true);
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            names.add(new Name("Ivanov","Ivan","Ivanovich"));
            myAdapterSpinner = new MyAdapterSpinner(this, names);
            myAdapterSpinner.getName(0);
            spinner.setAdapter(myAdapterSpinner);
            break;

    }
}
    public void addData() {

        mGroupCollection = new ArrayList<GroupEntity>();
        for (int i = 1; i < 6; i++) {
            GroupEntity ge = new GroupEntity();
            ge.Name = "GroupName" + i;

           for (int j = 1; j < 5; j++) {
                GroupEntity.GroupItemEntity gi = ge.new GroupItemEntity();
                gi.Name = "Ivanov" + j;
                ge.GroupItemCollection.add(gi);
            }
            mGroupCollection.add(ge);
        }
    }
}




