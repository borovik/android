package com.borovyk.app;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dev3 on 2/19/14.
 */

public class GroupEntity {

    public String Name;
    public List<GroupItemEntity> GroupItemCollection;

    public GroupEntity()
    {
        GroupItemCollection = new ArrayList<GroupItemEntity>();
    }

    public class GroupItemEntity
    {
        public String Name;

    }
}
