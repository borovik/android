package com.borovyk.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

/**
 * Created by dev3 on 2/11/14.
 */

public class MainActivity extends Activity implements View.OnClickListener {

    Button button1;
    Button button2;
    Button button3;
    Button button4;
    Button button5;
    Button button6;

    public final static int LIST_VIEW=1;
    public final static int GRID_VIEW=2;
    public final static int SPINNER=3;
    public final static int LIST_FRAGMENT=5;
    public final static int EXPANDABLE_LIST_VIEW=6;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        button1=(Button)findViewById(R.id.button1);
        button1.setOnClickListener(this);
        button2=(Button)findViewById(R.id.button2);
        button2.setOnClickListener(this);
        button3=(Button)findViewById(R.id.button3);
        button3.setOnClickListener(this);
        button4=(Button)findViewById(R.id.button4);
        button4.setOnClickListener(this);
        button5=(Button)findViewById(R.id.button5);
        button5.setOnClickListener(this);
        button6=(Button)findViewById(R.id.button6);
        button6.setOnClickListener(this);

    }

    @Override
    public void onClick(View view){

        Bundle b=new Bundle();

        switch(view.getId()){

            case R.id.button1:
                Intent intent1=new Intent(this,SecondActivity.class);
                b.putInt("key",LIST_VIEW);
                intent1.putExtras(b);
                startActivity(intent1);
                break;

            case R.id.button2:
                Intent intent2=new Intent(this,SecondActivity.class);
                b.putInt("key",GRID_VIEW);
                intent2.putExtras(b);
                startActivity(intent2);
                break;

            case R.id.button3:
                Intent intent3=new Intent(this,SecondActivity.class);
                b.putInt("key",SPINNER);
                intent3.putExtras(b);
                startActivity(intent3);
                break;

            case R.id.button4:
                Intent intent4=new Intent(this,MyListActivity.class);
                startActivity(intent4);
                break;

            case R.id.button5:
                Intent intent5=new Intent(this,SecondActivity.class);
                b.putInt("key",LIST_FRAGMENT);
                intent5.putExtras(b);
                startActivity(intent5);
                break;

            case R.id.button6:
                Intent intent6=new Intent(this,SecondActivity.class);
                b.putInt("key",EXPANDABLE_LIST_VIEW);
                intent6.putExtras(b);
                startActivity(intent6);
                break;
        }
    }
}
