package com.borovyk.app;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.ListFragment;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.GridView;

import android.widget.ListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.Spinner;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by dev3 on 2/11/14.
 */

public class SecondActivity extends Activity {

    public final static int LIST_VIEW=1;
    public final static int GRID_VIEW=2;
    public final static int SPINNER=3;
    public final static int LIST_FRAGMENT=5;
    public final static int EXPANDABLE_LIST_VIEW=6;

    String[] names={"Name1","Name2","Name3","Name4","Name5","Name6","Name7"};

    String[] names2={"Name11","Name22","Name33","Name44","Name55","Name66","Name77"};

    String[] names3={"Name111","Name222","Name333","Name444","Name555","Name666","Name777"};

    String[] groups = new String[] {"NameGroups1", "NameGroups2", "NameGroups3"};


    ArrayList<Map<String, String>> groupData;
    ArrayList<Map<String, String>> childDataItem;
    ArrayList<ArrayList<Map<String, String>>> childData;
    Map<String, String> m;


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);

        ListView lvView=(ListView)findViewById(R.id.lvMain);
        GridView gridView=(GridView)findViewById(R.id.grView);
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        ExpandableListView elvView = (ExpandableListView) findViewById(R.id.elvView);

        lvView.setEnabled(false);
        gridView.setEnabled(false);
        spinner.setEnabled(false);
        elvView.setEnabled(false);

        Bundle b = getIntent().getExtras();


        switch (b.getInt("key")){

            case LIST_VIEW:
               lvView.setEnabled(true);
               ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,names);
               lvView.setAdapter(adapter);
                break;

            case GRID_VIEW:
                gridView.setEnabled(true);
                ArrayAdapter<String> adapter1=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,names2);
                gridView.setAdapter(adapter1);
                break;

            case SPINNER:
                spinner.setEnabled(true);
                ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,names3);
                adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter2);
                spinner.setPrompt("Title_name");
                spinner.setSelection(1);
                break;

            case LIST_FRAGMENT:
                spinner.setEnabled(false);
                ListFragment MyListFragment=new ListFragment();
                FragmentTransaction ft=getFragmentManager().beginTransaction();
                ft.add(R.id.container,MyListFragment);
                ft.commit();
                ArrayAdapter adapter3=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, names);
                MyListFragment.setListAdapter(adapter3);
                break;

            case EXPANDABLE_LIST_VIEW:
                spinner.setEnabled(false);
                elvView.setEnabled(true);
                groupData = new ArrayList<Map<String, String>>();
                for (String group : groups) {
                    m = new HashMap<String, String>();
                    m.put("groupName", group);
                    groupData.add(m);
                }
                String groupFrom[] = new String[] {"groupName"};
                int groupTo[] = new int[] {android.R.id.text1};
                childData = new ArrayList<ArrayList<Map<String, String>>>();
                childDataItem = new ArrayList<Map<String, String>>();
                for (String Name : names) {
                    m = new HashMap<String, String>();
                    m.put("Name", Name);
                    childDataItem.add(m);
                }
                childData.add(childDataItem);
                childDataItem = new ArrayList<Map<String, String>>();
                for (String Name : names2) {
                    m = new HashMap<String, String>();
                    m.put("Name", Name);
                    childDataItem.add(m);
                }
                childData.add(childDataItem);
                childDataItem = new ArrayList<Map<String, String>>();
                for (String Name : names3) {
                    m = new HashMap<String, String>();
                    m.put("Name", Name);
                    childDataItem.add(m);
                }
                childData.add(childDataItem);
                String childFrom[] = new String[] {"Name"};
                int childTo[] = new int[] {android.R.id.text1};

                SimpleExpandableListAdapter adapter5 = new SimpleExpandableListAdapter(this,groupData,
                        android.R.layout.simple_expandable_list_item_1,
                        groupFrom,
                        groupTo,
                        childData,
                        android.R.layout.simple_list_item_1,
                        childFrom,
                        childTo);
                elvView.setAdapter(adapter5);
                break;
        }
    }
}
