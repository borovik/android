package com.borovik.app;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by dev3 on 2/7/14.
 */
public class MainActivity extends Activity implements View.OnClickListener {

    Button button;

    public static final String ACTION="com.borovik.action.NEW_MASSAGE";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(ACTION);
        intent.putExtra("messagename","MessageName");
        sendBroadcast(intent);
    }
}
