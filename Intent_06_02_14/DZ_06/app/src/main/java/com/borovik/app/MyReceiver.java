package com.borovik.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

/**
 * Created by dev3 on 2/13/14.
 */

public class MyReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if(intent.getAction().equals("com.borovik.action.NEW_MASSAGE")){
            Bundle extras=intent.getExtras();
            Toast.makeText(context,"Message_Receive : "+ extras,Toast.LENGTH_SHORT).show();
        }
    }
}
