package com.borovik.app;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Created by dev3 on 2/7/14.
 */


public class MainActivity extends Activity implements View.OnClickListener {

    Button button;
    TextView textView;

    public static final String ACTION="com.borovik.action.NEW_MASSAGE";
    private BroadcastReceiver rec;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(this);

        textView=(TextView)findViewById(R.id.textView);
        textView.setText("Broadcust_Message");
    }

    @Override
    public void onClick(View view) {

        Intent intent = new Intent(ACTION);
        intent.putExtra("messagename","MessageName");
        sendBroadcast(intent);
    }

    @Override
    protected void onResume(){

        rec=new BroadcastReceiver(){

            @Override
            public void onReceive(Context context, Intent intent){

                intent.getStringExtra(ACTION);
            }
        };

        IntentFilter filter=new IntentFilter(ACTION);
        registerReceiver(rec,filter);
    }

    @Override
    protected void onDestroy(){
         super.onDestroy();
         unregisterReceiver(rec);
     }
}
