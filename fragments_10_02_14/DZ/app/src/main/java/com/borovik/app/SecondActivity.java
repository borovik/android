package com.borovik.app;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;

/**
 * Created by dev3 on 2/14/14.
 */

public class SecondActivity extends FragmentActivity {

    public static final String ACTION1="com.borovik.action.Color1";
    public static final String ACTION2="com.borovik.action.Color2";

    Fragment1 fragment1;
    Fragment2 fragment2;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);

          IntentFilter filter=new IntentFilter(ACTION1);
          filter.addAction(ACTION2);

  //        Intent intent=getIntent();

  //        String extras=intent.getStringExtra("");
 //         int color1 = Color.parseColor(extras);

          fragment1=new Fragment1();
          FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
          ft.add(R.id.frame1,fragment1);
          ft.addToBackStack(null);
          ft.commit();
       //   fragment1.setBGColore();

          fragment2 = new Fragment2();
          FragmentTransaction ft1 = getSupportFragmentManager().beginTransaction();
          ft1.add(R.id.frame2,fragment2);
          ft1.addToBackStack(null);
          ft1.commit();
     //     fragment2.setBGColore();


    }


    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }
}
