package com.borovik.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by dev3 on 2/14/14.
 */

public class MainActivity extends Activity implements View.OnClickListener {

    Button button;
    EditText editText1;
    EditText editText2;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        button=(Button)findViewById(R.id.btn1);
        button.setOnClickListener(this);

        editText1=(EditText)findViewById(R.id.edText1);
        editText2=(EditText)findViewById(R.id.edText2);
    }

    @Override
    public void onClick(View view) {

        Intent intent=new Intent(this,SecondActivity.class).putExtra("Color1",editText1.getText().toString())
                .putExtra("Color2",editText2.getText().toString());
        startActivity(intent);
    }
}
