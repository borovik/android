package com.borovik.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by dev3 on 2/18/14.
 */

public class Fragment1 extends Fragment {

    public static final String ACTION1="com.borovik.action.Color1";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment1,null) ;
        view.setBackgroundColor(Color.parseColor("#FF0000"));
        return view;
    }

    public void setBGColore(View view){
        view.setBackgroundColor(Color.parseColor("#FF0000"));
    }

    BroadcastReceiver br=new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent.getAction().equals("com.borovik.action.Color1")){

                Thread t = new Thread(new Runnable() {
                    public void run() {


                    }
                });
                t.start();

        /*            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    Fragment fragment1=new Fragment();
                    ft.add(R.id.frame1,fragment1);
                    ft.addToBackStack(null);
                    ft.commit();

                    String extras=intent.getStringExtra("Color1");
                    int color1 = Color.parseColor(extras);
                    fragment1.getView().setBackgroundColor(color1);
        */
            }
        }
    };
  //  registerReceiver(br,filter);
 }

