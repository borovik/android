package com.borvyk.app;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

/**
 * Created by dev3 on 2/10/14.
 */
public class SecondActivity extends FragmentActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_name);

        Fragment1 frag1=new Fragment1();
        Fragment2 frag2=new Fragment2();

        FragmentTransaction fTranc = getFragmentManager().beginTransaction();
        fTranc.add(R.id.frgmCont,frag1);
        fTranc.add(R.id.frgmCont,frag2);

        fTranc.addToBackStack(null);
        fTranc.commit();
    }
}
