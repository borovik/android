package com.borvyk.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by dev3 on 2/10/14.
 */

public class MainActivity extends FragmentActivity implements View.OnClickListener {

    Button button1;
    Button button2;
    EditText editText;
    TextView textView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        button1=(Button) findViewById(R.id.button1);
        button1.setOnClickListener(this);

        button2=(Button) findViewById(R.id.button2);
        button2.setOnClickListener(this);

        editText=(EditText)findViewById(R.id.edittext);
        editText.setOnClickListener(this);

        textView=(TextView) findViewById(R.id.textView);
        textView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.button2:
                Intent intent = new Intent(this,SecondActivity.class);
                startActivity(intent);

            case R.id.button1:
                textView.setText(editText.getText().toString());
        }
    }
}
