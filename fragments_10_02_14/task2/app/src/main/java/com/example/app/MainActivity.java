package com.example.app;


import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.app.FragmentTransaction;

/**
 * Created by dev3 on 2/10/14.
 */

public class MainActivity extends FragmentActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

       Fragment1 fragment1=new Fragment1();

       FragmentManager fragmentManager = getFragmentManager();
       FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
       fragmentTransaction.add(R.id.container1,fragment1);
       fragmentTransaction.addToBackStack("fragment1");
       fragmentTransaction.commit();

    }
}
