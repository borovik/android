package com.example.app;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by dev3 on 2/14/14.
 */
public class Fragment2 extends Fragment {

    Button btn2;

    private View.OnClickListener inputClickListener = new View.OnClickListener() {
        public void onClick(View v) {

            FragmentManager fragmentManager = getFragmentManager();
            Fragment3 fragment3=new Fragment3();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.container1,fragment3 );
            transaction.addToBackStack(null);
            transaction.commit();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.fragment2, null);

        btn2= (Button)contentView.findViewById(R.id.btn2);
        return contentView;
    }

    @Override
    public void onStart() {
        super.onStart();
        btn2.setOnClickListener(inputClickListener);
    }
}
