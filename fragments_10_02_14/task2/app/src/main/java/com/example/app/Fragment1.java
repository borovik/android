package com.example.app;

import android.app.Fragment;
import android.app.FragmentManager;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by dev3 on 2/14/14.
 */
public class Fragment1 extends Fragment {

    Button btn1;

    private View.OnClickListener inputClickListener = new View.OnClickListener() {
        public void onClick(View v) {

            FragmentManager fragmentManager = getFragmentManager();
            Fragment2 fragment2=new Fragment2();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.container1,fragment2);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.fragment1,null);

        btn1= (Button)contentView.findViewById(R.id.btn1);
        return contentView;
    }

    @Override
    public void onStart() {
        super.onStart();
        btn1.setOnClickListener(inputClickListener);
    }
}
