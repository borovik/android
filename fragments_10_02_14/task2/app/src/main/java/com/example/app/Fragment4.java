package com.example.app;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by dev3 on 2/14/14.
 */
public class Fragment4 extends Fragment {

    Button btn4;

    private View.OnClickListener inputClickListener = new View.OnClickListener() {
        public void onClick(View v) {

            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            fragmentManager.popBackStack("fragment1",FragmentManager.POP_BACK_STACK_INCLUSIVE);
            transaction.commit();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.fragment4, null);

        btn4= (Button)contentView.findViewById(R.id.btn4);

        return contentView;
    }

    @Override
    public void onStart() {
        super.onStart();
        btn4.setOnClickListener(inputClickListener);
    }
}
